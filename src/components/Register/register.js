import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";

// import axios from "axios";
import "./register.scss";

function Register(props) {
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [userName, setUsername] = useState("");
  const [password, setPassWord] = useState("");
  const [confirmPassWord, setConfirmPassWord] = useState("");
  const [isValid, setIsValid] = useState(true);
  const defaultValidInput = {
    isValidEmail: true,
    isValidPhone: true,
    isValidPassword: true,
    isValidConfirmPassword: true,
  };
  const [objValidateInput, setObjValidateInput] = useState(defaultValidInput);

  const history = useHistory();
  const backToLogin = () => {
    history.push("/login");
  };
  const validateInput = () => {
    setObjValidateInput(defaultValidInput);
    if (!email) {
      toast.error("Please input your email address");
      return false;
    }
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const isValid = emailRegex.test(email);
    setIsValid(isValid);
    if (!isValid) {
      setObjValidateInput({ ...defaultValidInput, isValidEmail: false });
      toast.error("Please input the valid email address");
      return false;
    }
    if (!phone) {
      setObjValidateInput({ ...defaultValidInput, isValidPhone: false });
      toast.error("Please input your phone number");
      return false;
    }
    if (!password) {
      setObjValidateInput({ ...defaultValidInput, isValidPassword: false });
      toast.error("Please input your password");
      return false;
    }
    if (password !== confirmPassWord) {
      setObjValidateInput({
        ...defaultValidInput,
        isValidConfirmPassword: false,
      });
      toast.error("the password is not the same");
      return false;
    }

    return true;
  };
  const handleRegister = () => {
    if (validateInput()) {
      let registerData = {
        email,
        phone,
        userName,
        password,
        confirmPassWord,
      };
      axios
        .post("http://localhost:8800/api/v1/register", registerData)
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  //call api

  return (
    <div className="register-container p-3">
      <div className="container">
        <div className="row">
          <div className="content-left col-7 d-none d-sm-block">
            <div className="brand">Facebook</div>
            <div className="content">
              Facebook helps you connect and share with the people in your life.
            </div>
          </div>
          <div className="content-right yellow col-12 col-sm-5 d-flex flex-column gap-3">
            <div className="brand d-sm-none text-center">Facebook</div>

            <div className="form-group">
              <label>Email:</label>
              <input
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                type="email"
                className={
                  objValidateInput.isValidEmail
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Email address"
              ></input>
            </div>

            <div className="form-group">
              <label>Phone number:</label>
              <input
                value={phone}
                onChange={(e) => {
                  setPhone(e.target.value);
                }}
                type="text"
                className={
                  objValidateInput.isValidPhone
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Phone number"
              ></input>
            </div>

            <div className="form-group">
              <label>User name:</label>
              <input
                value={userName}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
                type="text"
                className="form-control"
                placeholder="User name"
              ></input>
            </div>

            <div className="form-group">
              <label>Password:</label>
              <input
                value={password}
                onChange={(e) => {
                  setPassWord(e.target.value);
                }}
                type="password"
                className={
                  objValidateInput.isValidPassword
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Password"
              ></input>
            </div>

            <div className="form-group">
              <label>Re-type password</label>
              <input
                value={confirmPassWord}
                onChange={(e) => {
                  setConfirmPassWord(e.target.value);
                }}
                type="password"
                className={
                  objValidateInput.isValidConfirmPassword
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Re-type password"
              ></input>
            </div>

            <button
              className="btn btn-primary"
              onClick={() => {
                handleRegister();
              }}
            >
              Register
            </button>

            <hr />

            <div className="text-center">
              <button
                className="btn btn-success col-12 col-lg-6 "
                onClick={() => {
                  backToLogin();
                }}
              >
                Back to login
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
