import "./Nav.scss";
import { NavLink } from "react-router-dom";

function Navigation() {
  return (
    <div className="topnav">
      <NavLink className="active" to="/">
        Home
      </NavLink>
      <NavLink to="/news">News</NavLink>
      <NavLink to="/contact">Contact</NavLink>
      <NavLink to="/about">About</NavLink>
    </div>
  );
}
export default Navigation;
