import "./login.scss";
import { useHistory } from "react-router-dom";

const Login = (props) => {
  const history = useHistory();
  const createNewAccount = () => {
    history.push("/register");
  };
  return (
    <div className="login-container p-3">
      <div className="container">
        <div className="row">
          <div className="content-left col-7 d-none d-sm-block">
            <div className="brand">Facebook</div>
            <div className="content">
              Facebook helps you connect and share with the people in your life.
            </div>
          </div>
          <div className="content-right yellow col-12 col-sm-5 d-flex flex-column gap-3">
            <div className="brand d-sm-none text-center">Facebook</div>
            <input
              type="text"
              className="form-control"
              placeholder="Email address or phone number"
            ></input>
            <input
              type="password"
              className="form-control"
              placeholder="Password"
            ></input>
            <button className="btn btn-primary">Login</button>
            <span className="text-center">
              <a className="forgotPassword" href="/#">
                Forgot your password?
              </a>
            </span>
            <hr />
            <div className="text-center">
              <button
                className="btn btn-success col-12 col-lg-6 "
                onClick={() => {
                  createNewAccount();
                }}
              >
                Create new account
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Login;
